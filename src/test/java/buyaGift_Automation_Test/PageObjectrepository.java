/**
 * 
 */
package buyaGift_Automation_Test;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * @author Kaeshiker
 * This class will store all the locators and methods of login page
 */
public class PageObjectrepository 

{
	WebDriver driver;
	By SearchTextBox = By.xpath(".//nav[@id='menu']//ul//li[contains(@class,'nav-food--drink submenu')]//a[@href='/food-and-drink']//span[contains(text(),'Food & Drink')]");
	

	public PageObjectrepository(WebDriver driver)
	
	{
		this.driver=driver;	
		
	}
	public void typeSearch()
	{
		
		driver.findElement(By.xpath(".//*[@id='search-field']")).sendKeys("Food");
	}
	public void ClickOnSearchButton()
	{
		driver.findElement(By.xpath(".//span[@id='magnifier-search']")).click();
		
	}
	
	public void ScrollDown()
	{
		
		JavascriptExecutor je=(JavascriptExecutor)driver;
		je.executeScript("window.scrollBy(0,400)");
	}
	
	public void Product()
	
	{
		
			driver.findElement(By.xpath(".//div[@class='producttitle'][contains(text(),'Taste of Italy - Smartbox by Buyagift')]")).click();
			
		
	}
	
	
	
	public void BuyNow()
	
	{
		driver.findElement(By.xpath(".//div[@class='buynow-top']//form[@id='product-form']//div[@class='product-buynow']//button[@type='submit']")).click();
		
	}
	
   public void Verify_Page_content_Number_of_Added_Product()
   
   {
	   if (driver.getPageSource().contains("You have 1 item(s) in your basket"))
		   
	   {
		   System.out.println("Number Of Items selected is Displyed in the Basket");
	   }
	   else{
		   
		   System.out.println("Number Of Items Not Displyed in the Basket");
	   }
	   

   }
public void Verify_Page_content2_Prodcut_Prize()
   
   {
	   if (driver.getPageSource().contains("�29.99"))
		   
	   {
		   System.out.println("Product Prize Displayed in the Basket");
	   }
	   else{
		   
		   System.out.println("Product Prize Not Displayed in the Basket");
	   }
   }
	   public void Verify_Page_Prodcut()
	   
	   {
		   if (driver.getPageSource().contains("Taste of Italy - Smartbox by Buyagift"))
			   
		   {
			   System.out.println("Selected Product Displayed in the Basket");
		   }
		   else{
			   
			   System.out.println("Product Not Present in the Basket");
		   }
   }

public void EVoucher_Radio_Button()

{
	driver.findElement(By.xpath(".//html//div[2]/label[1]/input[1]")).click();
}
public void Personal_Message_TextArea() throws Exception

{
	//driver.findElement(By.xpath(".//*[starts-with(@class, 'form-control ng-pristine ng-valid ng-valid-maxlines ng-touched')]")).sendKeys("Please Add Sausage, caramelized onions, and fennel.");
	//driver.findElements(By.cssSelector(".div.ng-scope:nth-child(2) div.container-fluid:nth-child(1) div.row section.col-sm-12 div.container div.basket-contents div.row:nth-child(1) section.col-sm-8 div.ng-scope form.form-inline.ng-pristine.ng-valid.ng-valid-maxlines div.basket_product.animate-fadeIn.ng-scope:nth-child(1) div.holder.ng-scope div.row.additional_details:nth-child(2) div.basket_step.col-sm-6.step2.ng-scope:nth-child(2) div.form-group.col-xs-12.personalised_as_message.additional_details_panel > textarea.form-control.ng-pristine.ng-untouched.ng-valid.ng-valid-maxlines:nth-child(2))]")).sendKeys("Please Add Sausage, caramelized onions, and fennel.");
	//List<WebElement> list = driver.findElements(By.cssSelector(".div.ng-scope:nth-child(2) div.container-fluid:nth-child(1) div.row section.col-sm-12 div.container div.basket-contents div.row:nth-child(1) section.col-sm-8 div.ng-scope form.form-inline.ng-pristine.ng-valid.ng-valid-maxlines div.basket_product.animate-fadeIn.ng-scope:nth-child(1) div.holder.ng-scope div.row.additional_details:nth-child(2) div.basket_step.col-sm-6.step2.ng-scope:nth-child(2) div.form-group.col-xs-12.personalised_as_message.additional_details_panel > textarea.form-control.ng-pristine.ng-untouched.ng-valid.ng-valid-maxlines:nth-child(2))]"));
	//List<WebElement> list = driver.findElements(By.xpath(".//html//div[1]/div[1]/div[2]/div[2]/div[1]/textarea[1]"));
	List<WebElement> list = driver.findElements(By.xpath(".//textarea[@class='form-control ng-pristine ng-untouched ng-valid ng-valid-maxlines']"));
	System.out.println(list.size());
	list.get(0).click();
	list.get(0).sendKeys("Please Add Sausage, caramelized onions, and fennel.");
	
}
//public void Personal_Message_TextArea_Click()

//{
	//driver.findElement(By.xpath(".//*[starts-with(@class, 'form-control ng-pristine ng-valid ng-valid-maxlines ng-touched')]")).click();
	//driver.findElement(By.cssSelector(".div.ng-scope:nth-child(2) div.container-fluid:nth-child(1) div.row section.col-sm-12 div.container div.basket-contents div.row:nth-child(1) section.col-sm-8 div.ng-scope form.form-inline.ng-pristine.ng-valid.ng-valid-maxlines div.basket_product.animate-fadeIn.ng-scope:nth-child(1) div.holder.ng-scope div.row.additional_details:nth-child(2) div.basket_step.col-sm-6.step2.ng-scope:nth-child(2) div.form-group.col-xs-12.personalised_as_message.additional_details_panel > textarea.form-control.ng-pristine.ng-untouched.ng-valid.ng-valid-maxlines:nth-child(2))]")).click();
//}
public void Verify_Page_Prodcut_Basket_Summary(){

boolean isDisplayed = driver.findElement(By.xpath(".//span[@class='summary_prd_ttl col-xs-8 ng-binding']")).isDisplayed();
if (isDisplayed){

	System.out.println("Selected Product is displayed in the Basket Summary");
     
}
else{
	System.out.println("Selected Product is displayed in the Basket Summary");
}
			   
		 
}

//Verify that correct Product Prize is displayed in the basket summary

public void Product_Prize_in_Basket_Summary(){

boolean isDisplayed = driver.findElement(By.xpath(".//span[@class='summary_prd_price col-xs-4 ng-binding']")).isDisplayed();
if (isDisplayed){

	System.out.println("Product Prize is displayed correctly in the Basket Summary");
     
}
else{
	System.out.println("Product Prize is Not displayed correctly in the Basket Summary");
}
}		   
//Verify that correct Total Item is displayed in the basket summary

public void Total_Items_In_Basket_Summary(){

boolean isDisplayed = driver.findElement(By.xpath(".//div[@class='row basket_totals']//span[@class='cost_price col-xs-4 ng-binding']")).isDisplayed();
if (isDisplayed){

	System.out.println("correct Total Item is displayed in the basket summary");
   
}
else{
	System.out.println(" Worng Total Item is in the basket summary");
} 
}

public void Delivery_DropDown()

{

Select objSelect = new Select(driver.findElement(By.xpath(".//select[@ng-model='BasketViewModel.FkDeliveryMethodId']")));
objSelect.equals("E-voucher (Free)");
objSelect.selectByVisibleText("E-voucher (Free)");
}

	//Verify that the Packaging Total is displayed as 0 in Basket_Summary
	
	public void Packaging_Total_in_Basket_Summary(){
	
	boolean isDisplayed = driver.findElement(By.xpath(".//div[@class='packaging_totals ng-scope']//span[@class='cost_price col-xs-4 ng-binding']")).isDisplayed();
	if (isDisplayed){
	
		System.out.println("Packaging Total is displayed as 0 in Basket_Summary");
	   
	}
	else{
		System.out.println("Packaging Total is Not displayed as 0 in Basket_Summary");
	}
	}
	
	//Verify that the Packaging Total is displayed as 0 in Basket_Summary
	
	public void Delivery_Total_in_Basket_Summary(){
	
	boolean isDisplayed = driver.findElement(By.xpath(".//div[@class='delivery_totals']//span[@class='cost_price col-xs-4 ng-binding']")).isDisplayed();
	if (isDisplayed){
	
		System.out.println("Delivery Total is displayed as 0 in Basket_Summary-->As Expected");
	 
	}
	else{
		System.out.println("Delivery Total is Not displayed as 0 in Basket_Summary-->As Expected");
	}
	}
	
	//Verify that the Final Total is displayed correctly in Basket_Summary
	
	public void Final_Total_in_Basket_Summary(){
	
	String actualData = driver.findElement(By.xpath(".//div[@class='row final_totals']//span[@class='cost_price col-xs-4 ng-binding']")).getText();
	String expectedData = "�29.99";
	if (actualData.equals(expectedData)){
	    
		System.out.println("Final Total is displayed correctly in Basket_Summary");
	
	}
	else{
		System.out.println("Final Total is Not displayed correctly in Basket_Summary");
	}
	}

//PAY SECURLY NOW Button 
public void Pay_Securly_Now_Button()

{
	driver.findElement(By.xpath("./html[1]/body[1]/div[3]/div[1]/div[1]/div[1]/div[1]/div[1]/section[1]/div[1]/div[1]/div[1]/section[2]/div[1]/div[2]/a[1]/button[1]")).click();
}



//Enter email id

public void Email_Id()

{
	driver.findElement(By.xpath(".//input[@id='account_email_field']")).sendKeys("kaeshikerj@gmail.com");
}




//Verify that the Packaging Total is displayed as 0 in Checkout_Summary

public void Checkout_Packaging_Total(){

boolean isDisplayed = driver.findElement(By.xpath(".//div[@class='packaging_totals ng-scope']//span[@class='cost_price col-xs-4 ng-binding']")).isDisplayed();
if (isDisplayed){

	System.out.println("Packaging Total is displayed as 0 in Checkout_Summary");
 
}
else{
	System.out.println("Packaging Total is Not displayed as 0 in Checkout_Summary");
}
}

//Verify that the Packaging Total is displayed as 0 in Checkout_Summary

public void Checkout_Delivery_Total(){

boolean isDisplayed = driver.findElement(By.xpath(".//div[@class='delivery_totals']//span[@class='cost_price col-xs-4 ng-binding']")).isDisplayed();
if (isDisplayed){

	System.out.println("Delivery Total is displayed as 0 in Checkout_Summary-->As Expected");

}
else{
	System.out.println("Delivery Total is Not displayed as 0 in Checkout_Summary-->As Expected");
}
}

//Verify that the Final Total is displayed correctly in Checkout_Summary

public void Checkout_Final_Total(){

String actualData = driver.findElement(By.xpath(".//div[@class='row final_totals']//span[@class='cost_price col-xs-4 ng-binding']")).getText();
String expectedData = "�29.99";
if (actualData.equals(expectedData)){
  
	System.out.println("Final Total is displayed correctly in Checkout_Summary");

}
else{
	System.out.println("Final Total is Not displayed correctly in Checkout_Summary");
}
}

//Click on CONTINUE AS GUEST

//Enter email id

public void Continue_As_Gust()

{
	driver.findElement(By.xpath(".//html//div[@class='step1_btns col-md-offset-4']/button[2]")).click();
}

public void PersonalDetails_And_Continue()

{
	
	{
		Select objSelect = new Select(driver.findElement(By.xpath("./html[1]/body[1]/div[2]/div[1]/div[1]/div[1]/div[1]/section[1]/div[1]/div[1]/div[1]/div[1]/section[1]/div[1]/section[1]/div[1]/div[2]/div[1]/div[1]/div[1]/select[1]")));
		objSelect.equals("Please select");
		objSelect.selectByVisibleText("Mr.");
	}
		//driver.findElement(By.xpath("./html[1]/body[1]/div[2]/div[1]/div[1]/div[1]/div[1]/section[1]/div[1]/div[1]/div[1]/div[1]/section[1]/div[1]/section[1]/div[1]/div[2]/div[1]/div[1]/div[1]/select[1]")).();
		driver.findElement(By.xpath("./html[1]/body[1]/div[2]/div[1]/div[1]/div[1]/div[1]/section[1]/div[1]/div[1]/div[1]/div[1]/section[1]/div[1]/section[1]/div[1]/div[2]/div[1]/div[2]/div[1]/input[1]")).sendKeys("Kaeshiker");
		driver.findElement(By.xpath("./html[1]/body[1]/div[2]/div[1]/div[1]/div[1]/div[1]/section[1]/div[1]/div[1]/div[1]/div[1]/section[1]/div[1]/section[1]/div[1]/div[2]/div[1]/div[3]/div[1]/input[1]")).sendKeys("Jayasingh");
		driver.findElement(By.xpath("./html[1]/body[1]/div[2]/div[1]/div[1]/div[1]/div[1]/section[1]/div[1]/div[1]/div[1]/div[1]/section[1]/div[1]/section[1]/div[1]/div[2]/div[1]/div[4]/div[1]/input[1]")).sendKeys("08995289998");
		driver.findElement(By.xpath("./html[1]/body[1]/div[2]/div[1]/div[1]/div[1]/div[1]/section[1]/div[1]/div[1]/div[1]/div[1]/section[1]/div[1]/section[1]/div[1]/div[2]/div[2]/button[1]")).click();



}

public void Billing_Address() throws InterruptedException

{	
		driver.findElement(By.xpath(".//ui-view[@id='postCodeSearchBilling']//div[@ng-form='PostCodeSearchForm']//div[@class='form-group housenumbersearch']//div[@class='col-sm-3 nopadding']//input[@type='text']")).sendKeys("8");
		driver.findElement(By.xpath(".//ui-view[@id='postCodeSearchBilling']//div[@ng-form='PostCodeSearchForm']//div[@class='form-group postcodesearch']//div[@class='col-sm-3 nopadding']//input[@type='text']")).sendKeys("EC1A1BB");
		//driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		Thread.sleep(5000);
		//Wait();
		driver.findElement(By.xpath(".//ui-view[@id='postCodeSearchBilling']//div[@ng-form='PostCodeSearchForm']//div[@class='form-group postcodesearch']//div[@class='continue_area_buttons']//button[@type='button']")).click();
}

public void Wait()

{
	
WebDriverWait wait =  new WebDriverWait(driver,100);
long t1 = System.currentTimeMillis();
try {
	wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//ui-view[@id='postCodeSearchBilling']//div[@ng-form='PostCodeSearchForm']//div[@class='form-group postcodesearch']//div[@class='continue_area_buttons']//button[@type='button']")));
}
catch(Exception e){
	System.out.println("Element is clickable");
}
long t2 = System.currentTimeMillis();
double timetaken = (t2-t1)/1000;
System.out.print("Done! " + timetaken);

}

public void Enter_Payment_Details_And_Place_Order() throws InterruptedException

{	
		driver.findElement(By.xpath(".//input[@id='cardholdername']")).sendKeys("Kevin");
		driver.findElement(By.xpath(".//input[@id='cardnumber']")).sendKeys("4111111111111111");
		//driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		//Thread.sleep(5000);
		//Wait();
		ScrollDown();
		
		{

			Select objSelect1 = new Select(driver.findElement(By.id("expirymonth")));
			objSelect1.equals("MM");
			objSelect1.selectByVisibleText("5");
			
			Select objSelect2 = new Select(driver.findElement(By.id("expiryyear")));
			objSelect2.equals("YYYY");
			objSelect2.selectByVisibleText("2020");
			
			Select objSelect3 = new Select(driver.findElement(By.id("startmonth")));
			objSelect3.equals("MM");
			objSelect3.selectByVisibleText("7");
			
			Select objSelect4 = new Select(driver.findElement(By.id("startyear")));
			objSelect4.equals("YYYY");
			objSelect4.selectByVisibleText("2017");
			
			driver.findElement(By.id("issuenumber")).sendKeys("123");
			driver.findElement(By.id("cvvnumber")).sendKeys("123");
			ScrollDown();
			Thread.sleep(5000);
			driver.findElement(By.id("btnPlaceOrderButton")).click();
			
			}
		//driver.findElement(By.xpath(".//ui-view[@id='postCodeSearchBilling']//div[@ng-form='PostCodeSearchForm']//div[@class='form-group postcodesearch']//div[@class='continue_area_buttons']//button[@type='button']")).click();
}

public void Error_for_fake_Credit_Card(){

String actualData = driver.findElement(By.xpath(".//span[@class='error ng-binding ng-scope']")).getText();
String expectedData = "The card details were not authorised by the issuing bank. Please check the details and try again.";
if (actualData.equals(expectedData)){
  
	System.out.println("Error message is displayed for Fake credit card as expected");

}
else{
	System.out.println("Error message is NOT displayed for Fake credit card as expected");
}
}




















}


