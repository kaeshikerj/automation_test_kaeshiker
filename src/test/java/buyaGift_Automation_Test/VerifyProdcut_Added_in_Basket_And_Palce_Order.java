/**
 * 
 */
package buyaGift_Automation_Test;


/**
 * @author Kaeshiker
 * 
 * This class is to verify that the user is able to search for the favourite product
 * and verify the selected product are added with correct prize
 *  
 *
 */
import java.util.concurrent.TimeUnit;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.AssertJUnit;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

@Listeners(buyaGift_Automation_Test.TestNGListerner.class)
public class VerifyProdcut_Added_in_Basket_And_Palce_Order {		
    private WebDriver driver;		
	@Test				
	public void testEasy() throws Exception {	
		
		//Add Chrome Driver path
		System.setProperty("webdriver.Chrome.driver","C:\\Selenium\\Chrome\\chromedriver_win32\\chromedriver.exe");

		//Open the login page
		driver.get("https://www.buyagift.co.uk/"); 
		
		//Verify the page Title using Assertions 
		String title = driver.getTitle();				 
		AssertJUnit.assertTrue(title.contains("Experience Days and Gifts from Buyagift")); 
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();
	    //Thread.sleep(5000);
		
		//call page object repository(POM)  to locate the login page elements 
		PageObjectrepository Enter=new PageObjectrepository(driver);
		
		//Thread.sleep(5000);
		Enter.typeSearch();
		Enter.ClickOnSearchButton();
		
		//driver.findElement(By.xpath(".//*[@id='search-field']")).sendKeys("Food");
		
		//Verify that the user is navigated to product  page after click on the search button on the login page
		// and verify that the user is able to add product in the baskets
		Thread.sleep(5000);
		String title2 = driver.getTitle();				 
		AssertJUnit.assertFalse(title2.contains("Food and Drink Gift Experiencies"));
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		//Scroll Down the page to find more product
		
		//JavascriptExecutor jse=(JavascriptExecutor)driver;
		//jse.executeScript("window.scrollBy(0,500)");
		//driver.get("https://www.buyagift.co.uk/food-and-drink?search=true");
		//((JavascriptExecutor)driver).executeScript("scroll(0,1000)");
		
		//Scroll Down to see the products
		PageObjectrepository scroll=new PageObjectrepository(driver);
		scroll.ScrollDown();
		
		//Click on the 
		
		PageObjectrepository prod=new PageObjectrepository(driver);
		
		prod.Product();
		
		// Click on BuyNow
		PageObjectrepository Buy=new PageObjectrepository(driver);
		
		Buy.BuyNow();
		
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		//Verify that the user is navigated to Add product to Basket page and selected product is added in the  Basket 
		String title3 = driver.getTitle();
		AssertJUnit.assertTrue(title3.contains("BAG Basket Page"));
		
		//Verify the Number of Product added to the Basket
		
        PageObjectrepository NumberOfProductContent=new PageObjectrepository(driver);
		
        NumberOfProductContent.Verify_Page_content_Number_of_Added_Product();
        
        //Verify that the correct Product is added in the basket
        
      //a[@href='/food-and-drink/smartbox-by-buyagift/taste-of-italy-smartbox-by-buyagift-br-10749545.aspx'][contains(text(),'Taste of Italy - Smartbox by Buyagift')]
        
        
       //Verify the Selected Product  is displayed in the Basket
        
        PageObjectrepository Selected_Product=new PageObjectrepository(driver);
        Selected_Product.Verify_Page_Prodcut();
        
        
        //Verify the Product prize is displayed in the Basket
        
        PageObjectrepository Product_Prize=new PageObjectrepository(driver);
        Product_Prize.Verify_Page_content2_Prodcut_Prize();
        
        //Select E-voucher radio button
        PageObjectrepository Evoucher_Button=new PageObjectrepository(driver);
        
        Evoucher_Button.EVoucher_Radio_Button();
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        Thread.sleep(5000);
        
      //Enter Personal message
       PageObjectrepository Personal_msg=new PageObjectrepository(driver);
       Personal_msg.Personal_Message_TextArea();
       
       //Verify that the correct selected product & Prize added in Basket Summary
       //Verify that additional delivery charge is Not added Basket Summary as this user selected E-voucher Options
       
       
       
       
        
        
        //TakesScreenshot ts=(TakesScreenshot)driver;

        //File source=ts.getScreenshotAs(OutputType.FILE);
       
       // FileUtils.copyFile(source, new File("./Screenshots/PersonalMessageTextArea.png"));
        
       
       //Verify the Selected Product  is displayed in the Basket
       
       //PageObjectrepository Selected_Product=new PageObjectrepository(driver);
       Selected_Product.Verify_Page_Prodcut_Basket_Summary();  
        
     //Verify that correct Product Prize is displayed in the basket summary
       Product_Prize.Product_Prize_in_Basket_Summary();
       
     //Verify that correct Total Item is displayed in the basket summary
       PageObjectrepository Total=new PageObjectrepository(driver);
       Total.Total_Items_In_Basket_Summary();
       
     //Scroll Down to see the products
     PageObjectrepository scroll_To_see_Basket_summary_total=new PageObjectrepository(driver);
     scroll_To_see_Basket_summary_total.ScrollDown();
     
     //Verify that the E-Voucher is selected as default from Delivery Drop down and select it again in the Basket summary
     PageObjectrepository Deliver_drop=new PageObjectrepository(driver);
     Deliver_drop.Delivery_DropDown();
     
     //Verify that the Packaging Total: is displayed as Zero
     
     PageObjectrepository Packaging_Total=new PageObjectrepository(driver);
     Packaging_Total.Packaging_Total_in_Basket_Summary();
     
     //Verify that the Delivery Total: is displayed as Zero
     
     PageObjectrepository Deliver_Total=new PageObjectrepository(driver);
     Deliver_Total.Packaging_Total_in_Basket_Summary();
     
     //Verify that the Delivery Total: is displayed correctly without add any Packaging & Delivery charge
     
     PageObjectrepository Final_Total=new PageObjectrepository(driver);
     Final_Total.Final_Total_in_Basket_Summary();
     
     //Click on PAY SECURLY NOW Button
     
     PageObjectrepository Pay_Securly_Button=new PageObjectrepository(driver);
     Pay_Securly_Button.Pay_Securly_Now_Button();
     
    //Click on the OK button in the confirmation popups
     
     org.openqa.selenium.Alert alert = driver.switchTo().alert();
     driver.switchTo().alert();
     alert.accept();
     
     //Enter Email Id 
     PageObjectrepository email=new PageObjectrepository(driver);
     email.Email_Id();
     
     //Verify Product and Prize are correct in the CHECKOUT SUMMARY
     
     //Verify that the Packaging Total: is displayed as Zero
     
     PageObjectrepository Checkout_Packaging_Total=new PageObjectrepository(driver);
     Checkout_Packaging_Total.Checkout_Packaging_Total();
     
     //Verify that the Delivery Total: is displayed as Zero
     
     PageObjectrepository Checkout_Deliver_Total=new PageObjectrepository(driver);
     Checkout_Deliver_Total.Checkout_Packaging_Total();
     
     //Verify that the Delivery Total: is displayed correctly without add any Packaging & Delivery charge
     
     PageObjectrepository Checkout_Final_Total=new PageObjectrepository(driver);
     Checkout_Final_Total.Checkout_Final_Total();
     
     //Click on Continue as Guest
     
     PageObjectrepository Gust=new PageObjectrepository(driver);
     Gust.Continue_As_Gust();
     
     Thread.sleep(15000);
     
//Enter Personal Details
     
     PageObjectrepository Personal=new PageObjectrepository(driver);
     Personal.PersonalDetails_And_Continue();
     
//Enter Billing Address
     
     PageObjectrepository Billing_Add=new PageObjectrepository(driver);
     Billing_Add.Billing_Address();
     
//Enter Payments details with Fake credit card and Palce an order
     
     PageObjectrepository Payment_details_with_Fake_card=new PageObjectrepository(driver);     
     Payment_details_with_Fake_card.Enter_Payment_Details_And_Place_Order();
     
     Thread.sleep(5000);
     
//Verify the Error Displayed for Fake credit card number
     PageObjectrepository Error_message_for_Fake_Card=new PageObjectrepository(driver);     
     Error_message_for_Fake_Card.Error_for_fake_Credit_Card();
     
     
 
		

	}	
	//Initialize chrome driver object before test using TestNG TestNG annotations
	@BeforeTest
	public void beforeTest() {	
	    driver = new ChromeDriver();  
	}		
	//Quit the driver at the end of the Test
	@AfterTest
	public void afterTest() {
		
		//System.out.println("Hello");
		driver.quit();			
	}		
}	

