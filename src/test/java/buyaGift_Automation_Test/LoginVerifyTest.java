package buyaGift_Automation_Test;

import org.openqa.selenium.WebDriver;
/**
 * @author Kaeshiker
 * This class is to verify User is able to BuyaGift page and able to search for the favourite product
 */
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.AssertJUnit;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;



public class LoginVerifyTest {		
    private WebDriver driver;		
	@Test
	public void testEasy() {	
		
		//Add Chrome Driver path
		System.setProperty("webdriver.Chrome.driver","C:\\Selenium\\Chrome\\chromedriver_win32\\chromedriver.exe");

		//Open the login page
		driver.get("https://www.buyagift.co.uk/"); 
		
		//Verify the page Title using Assertions 
		String title = driver.getTitle();				 
		AssertJUnit.assertTrue(title.contains("Experience Days and Gifts from Buyagift")); 
		
		//call page object repository(POM)  to locate the login page elements 
		PageObjectrepository login=new PageObjectrepository(driver);
		
		login.typeSearch();
		login.ClickOnSearchButton();
		
		
	}	
	//Initialize chrome driver object before test using TestNG TestNG annotations
	@BeforeTest
	public void beforeTest() {	
	    driver = new ChromeDriver();  
	}		
	//Quit the driver at the end of the Test
	@AfterTest
	public void afterTest() {
		
		
		driver.quit();			
	}		
}	
