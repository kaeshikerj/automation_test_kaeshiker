
package CaptureScreenshot;
import java.util.concurrent.TimeUnit;

import library.Utility;



import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.AssertJUnit;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;



public class LoginScreenshot {		
    WebDriver driver;		
	@Test				
	public void FailCase() throws Exception {	
		
		//if you didn't update the Path system variable to add the full directory path to the executable as above mentioned then doing this directly through code
		System.setProperty("webdriver.Chrome.driver","C:\\Selenium\\Chrome\\chromedriver_win32\\chromedriver.exe");

		//Now you can Initialize marionette driver to launch firefox
		//DesiredCapabilities capabilities = DesiredCapabilities.firefox();
		//capabilities.setCapability("marionette", true);
		//WebDriver driver = new MarionetteDriver(capabilities);
		driver.get("https://www.buyagift.co.uk/"); 
		
		//Verify the page Title using Assertions 
				String title = driver.getTitle();				 
				AssertJUnit.assertTrue(title.contains("Experience Days and Gifts from Buyagift")); 
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				driver.manage().window().maximize();
		
		//String title = driver.getTitle();				 
		//assert.assertTrue(title.contains("Experience Days and Gifts from Buyagift")); 
		
		driver.findElement(By.xpath(".//*[@id='search-field']")).click();
		driver.findElement(By.xpath(".//*[@id='search-field']")).click();
		driver.findElement(By.xpath(".//*[@id='search-field']")).click();
		driver.findElement(By.xpath(".//*[@id='search-field']")).sendKeys("Kaeshiker");

		driver.findElement(By.xpath(".//nav[@id='menu']//ul//li[contains(@id,'nav-food--drink submenu')]//a[@href='/food-and-drink']//span[contains(text(),'Food & Drink')]")).click();
		
	}	
	@AfterMethod
	public void tearDown(ITestResult result) 
	
	{	
	    if(ITestResult.FAILURE==result.getStatus())
	    	
	    {
	    	
	    	Utility.captureScreenshot(driver, result.getName());
	    }
	}		
	
	@BeforeTest
	public void beforeTest() {	
	    driver = new ChromeDriver();  
	}	
	@AfterTest
	public void afterTest() {
		driver.quit();			
	}		
}	