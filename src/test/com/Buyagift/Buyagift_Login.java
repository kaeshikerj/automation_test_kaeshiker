
package example;

import junit.framework.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.AfterTest;

public class Login_BuyaGift {		
    private WebDriver driver;		
	@Test				
	public void testEasy() {	
		
		//if you didn't update the Path system variable to add the full directory path to the executable as above mentioned then doing this directly through code
		System.setProperty("webdriver.Chrome.driver","C:\\Selenium\\Chrome\\chromedriver_win32\\chromedriver.exe");

		//Now you can Initialize marionette driver to launch firefox
		//DesiredCapabilities capabilities = DesiredCapabilities.firefox();
		//capabilities.setCapability("marionette", true);
		//WebDriver driver = new MarionetteDriver(capabilities);
		driver.get("https://www.buyagift.co.uk/"); 
		
		String title = driver.getTitle();				 
		Assert.assertTrue(title.contains("Experience Days and Gifts from Buyagift")); 
		
		
	}	
	@BeforeTest
	public void beforeTest() {	
	    driver = new ChromeDriver();  
	}		
	@AfterTest
	public void afterTest() {
		driver.quit();			
	}		
}	